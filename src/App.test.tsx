import { fireEvent, render, waitFor } from "@testing-library/react";
import App from "./App";

describe("Testando componente da listagem", () => {
  it("Adicionando um novo item na lista", async () => {
    const { getByTestId, getByText } = render(<App />);

    const testCase = "Item teste";
    const formField = await waitFor(() => getByTestId("test-input"));
    fireEvent.change(formField, { target: { value: testCase } });
    expect(formField.value).toEqual(testCase);

    const button = await waitFor(() => getByTestId("test-btn"));
    fireEvent.click(button);

    const listItem = await waitFor(() => getByText(testCase));
    expect(listItem).toBeDefined();
  });

  it("Tentando adicionar sem um input", async () => {
    const { getByTestId } = render(<App />);

    const testCase = "";
    const formField = await waitFor(() => getByTestId("test-input"));
    fireEvent.change(formField, { target: { value: testCase } });
    expect(formField.value).toEqual(testCase);
    expect(getByTestId("test-btn")).toBeDisabled();
  });
});
