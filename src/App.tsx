import { FormEvent, useState } from "react";
import "./App.css";

const App = () => {
  const [userInput, setUserInput] = useState<string>("");
  const [lista, setLista] = useState<string[]>([]);

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    setLista([...lista, userInput]);
    setUserInput("");
  };

  return (
    <div className="App">
      <header className="App-header">
        <form onSubmit={handleSubmit} data-testid={"test-form"}>
          <input
            data-testid={"test-input"}
            type="text"
            onChange={(e) => setUserInput(e.target.value)}
            value={userInput}
          />
          <button
            data-testid={"test-btn"}
            type="submit"
            disabled={userInput !== "" ? false : true}
          >
            Enviar
          </button>
        </form>
        <ul data-testid={"test-list"}>
          {lista.map((item, index) => (
            <li key={index}>{item}</li>
          ))}
        </ul>
      </header>
    </div>
  );
};

export default App;
